import { createSlice } from "@reduxjs/toolkit";
import {
  getCongViecTheoChiTietLoai,
  getDanhSachCongViecTheoTen,
  getMenuLoaiCongViec,
} from "../thunk/congViecThunk";

const initialState = {
  danhSachCongViecTheoTen: [],
  menuLoaiCongViec: [],
  congViecTheoChiTiet: [],
  loadingCongViec: false,
};

export const congViecSlice = createSlice({
  name: "congViecSlice",
  initialState,

  reducers: {
    layDanhSachCongViec: (state, action) => {
      state.danhSachCongViecTheoTen = action.payload;
    },
    layMenuCongViec: (state, action) => {
      state.menuLoaiCongViec = action.payload;
    },
    layCongViecTheoChiTietLoai: (state, action) => {
      state.congViecTheoChiTiet = action.payload;
    },
  },

  extraReducers: (builder) => {
    // slice 1
    builder.addCase(getDanhSachCongViecTheoTen.pending, (state) => {
      state.loadingCongViec = true;
      state.danhSachCongViecTheoTen = [];
    });
    builder.addCase(
      getDanhSachCongViecTheoTen.fulfilled,
      (state, { payload }) => {
        state.loadingCongViec = false;
        state.danhSachCongViecTheoTen = payload;
      }
    );

    // slice 2
    builder.addCase(getMenuLoaiCongViec.pending, (state) => {
      state.loadingCongViec = true;
      state.menuLoaiCongViec = [];
    });
    builder.addCase(getMenuLoaiCongViec.fulfilled, (state, { payload }) => {
      state.loadingCongViec = false;
      state.menuLoaiCongViec = payload;
    });

    // slice 3
    builder.addCase(getCongViecTheoChiTietLoai.pending, (state) => {
      state.loadingCongViec = true;
      state.congViecTheoChiTiet = [];
    });
    builder.addCase(
      getCongViecTheoChiTietLoai.fulfilled,
      (state, { payload }) => {
        state.loadingCongViec = false;
        state.congViecTheoChiTiet = payload;
      }
    );
  },
});

export const {
  layDanhSachCongViec,
  layMenuCongViec,
  layCongViecTheoChiTietLoai,
} = congViecSlice.actions;

export default congViecSlice.reducer;
