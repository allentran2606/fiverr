import { createSlice } from "@reduxjs/toolkit";
import { postDangNhap } from "../thunk/userThunk";

const initialState = {
  userDangNhap: {},
  loadingUser: false,
};

export const userSlice = createSlice({
  name: "userSlice",
  initialState,

  reducers: {
    layThongTinUserDangNhap: (state, action) => {
      state.userDangNhap = action.payload;
    },
  },

  extraReducers: (builder) => {
    // Slice 1
    builder.addCase(postDangNhap.pending, (state) => {
      state.loadingUser = true;
      state.userDangNhap = {};
    });
    builder.addCase(postDangNhap.fulfilled, (state, { payload }) => {
      state.loadingUser = false;
      state.userDangNhap = payload;
    });
  },
});

export const { layThongTinUserDangNhap } = userSlice.actions;
export default userSlice.reducer;
