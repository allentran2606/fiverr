import { createAsyncThunk } from "@reduxjs/toolkit";
import { message } from "antd";
import { useNavigate } from "react-router-dom";
import { userService } from "../../services/userService";

export const postDangNhap = createAsyncThunk("postDangNhap", async (values) => {
  try {
    console.log(`values: `, values);
    const userData = await userService.layThongTinUserDangNhap(values);
    return userData.data.content;
  } catch ({ message }) {
    console.log(`message: `, message);
  }
});
