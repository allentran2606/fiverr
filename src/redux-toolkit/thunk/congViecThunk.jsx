import { createAsyncThunk } from "@reduxjs/toolkit";
import { congViecService } from "../../services/congViecService";

export const getDanhSachCongViecTheoTen = createAsyncThunk(
  "getDanhSachCongViecTheoTen",
  async (tenCongViec) => {
    try {
      const jobData = await congViecService.layDanhSachCongViecTheoTen(
        tenCongViec
      );
      return jobData.data.content;
    } catch ({ message }) {}
  }
);

export const getMenuLoaiCongViec = createAsyncThunk(
  "getMenuLoaiCongViec",
  async () => {
    try {
      const content = await congViecService.layMenuLoaiCongViec();
      return content.data.content;
    } catch ({ messsage }) {}
  }
);

export const getCongViecTheoChiTietLoai = createAsyncThunk(
  "getCongViecTheoChiTietLoai",
  async (maChiTietLoai) => {
    try {
      const content = await congViecService.layCongViecTheoChiTietLoai(
        +maChiTietLoai
      );
      return content.data.content;
    } catch ({ message }) {}
  }
);
