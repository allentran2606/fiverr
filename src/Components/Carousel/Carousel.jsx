import React from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-fade";

// import required modules
import { EffectFade, Autoplay } from "swiper";
import SearchBar from "../SearchBar/SearchBar";
import { Desktop, Laptop, Mobile, Tablet } from "../../HOC/Responsive";
import SearchBarTablet from "../SearchBar/SearchBarTablet";

export default function App() {
  return (
    <div className="carousel relative">
      {/* Image Backgrounds */}
      <div className="carousel-backgrounds">
        <Swiper
          spaceBetween={30}
          effect={"fade"}
          autoplay={{
            delay: 2500,
            disableOnInteraction: false,
          }}
          modules={[EffectFade, Autoplay]}
          className="mySwiper"
        >
          <SwiperSlide>
            <div className="hero-valentina w-full h-full bg-image-rule"></div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="hero-andrea w-full h-full bg-image-rule"></div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="hero-moon w-full h-full bg-image-rule"></div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="hero-ritika w-full h-full bg-image-rule"></div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="hero-zach w-full h-full bg-image-rule"></div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="hero-gabrielle w-full h-full bg-image-rule"></div>
          </SwiperSlide>
        </Swiper>
      </div>

      {/* Search Form */}
      <div className="carousel-search-bar flex items-center lg:container lg:mx-auto lg:h-full lg:pl-8 lg:justify-start lg:p-0 sm:justify-center sm:px-16 sm:py-24">
        <div className="header-search-bar">
          <Desktop>
            <SearchBar />
          </Desktop>
          <Laptop>
            <SearchBar />
          </Laptop>
          <Tablet>
            <SearchBarTablet />
          </Tablet>
          <Mobile>
            <SearchBarTablet />
          </Mobile>
        </div>
      </div>
    </div>
  );
}
