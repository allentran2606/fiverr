import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { Swiper, SwiperSlide } from "swiper/react";
import { getMenuLoaiCongViec } from "../../redux-toolkit/thunk/congViecThunk";
import DetailJobHover from "../DetailJobHover/DetailJobHover";
import { Scrollbar } from "swiper";
import "swiper/css";
import "swiper/css/effect-fade";

export default function CategoryMenu() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMenuLoaiCongViec());
  }, []);

  const listMenuJob = useSelector(
    (state) => state.congViecSlice.menuLoaiCongViec
  );

  const renderMenuCongViec = (listMenuJob) => {
    return listMenuJob.map((item, index) => {
      let listChiTietViec = item.dsNhomChiTietLoai;
      return (
        <SwiperSlide>
          <li className="flex-shrink-0">
            <div className="group inline-block relative text-left">
              <button className="text-gray-700 py-3 font-semibold rounded inline-flex items-center">
                <NavLink to={`/category/${item.id}`}>
                  <span className="text-left">{item.tenLoaiCongViec}</span>
                </NavLink>
              </button>
              <ul className="absolute z-20 hidden text-gray-700 group-hover:block bg-white w-80 text-left px-7 pb-6">
                <DetailJobHover listChiTietViec={listChiTietViec} />
              </ul>
            </div>
          </li>
        </SwiperSlide>
      );
    });
  };

  return (
    <div className="category-menu border-t border-b">
      <div className="category-menu-wrapper container mx-auto">
        <ul className="flex items-center justify-between w-full">
          <Swiper
            slidesPerView={7}
            slidesPerGroup={7}
            spaceBetween={20}
            scrollbar={{
              hide: false,
            }}
            modules={[Scrollbar]}
            breakpoints={{
              // when window width is >= 640px
              640: {
                slidesPerView: 3,
              },
              // when window width is >= 768px
              768: {
                slidesPerView: 4,
              },
              1280: {
                slidesPerView: 7,
              },
            }}
            className="mySwiper"
          >
            {listMenuJob !== undefined ? renderMenuCongViec(listMenuJob) : ""}
          </Swiper>
        </ul>
      </div>
    </div>
  );
}
