import React from "react";
import { NavLink } from "react-router-dom";

export default function Marketplace() {
  return (
    <div>
      <div className="container mx-auto marketplace">
        <h2 className="font-bold text-3xl text-left mb-10 pb-6">
          Explore the marketplace
        </h2>
        <ul className="categories-list grid grid-cols-5 sm:grid-cols-3 md:grid-cols-4">
          <li>
            <NavLink
              href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
              className="text-center category-item text-lg"
            >
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/graphics-design.d32a2f8.svg"
                alt="Graphics & Design"
                loading="lazy"
                className="w-12 h-12 block mx-auto mb-4"
              />
              Graphics &amp; Design
            </NavLink>
          </li>
          <li>
            <NavLink
              href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
              className="text-center category-item text-lg"
            >
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/online-marketing.74e221b.svg"
                alt="Digital Marketing"
                loading="lazy"
                className="w-12 h-12 block mx-auto mb-4"
              />
              Digital Marketing
            </NavLink>
          </li>
          <li>
            <NavLink
              href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
              className="text-center category-item text-lg"
            >
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/writing-translation.32ebe2e.svg"
                alt="Writing & Translation"
                loading="lazy"
                className="w-12 h-12 block mx-auto mb-4"
              />
              Writing &amp; Translation
            </NavLink>
          </li>
          <li>
            <NavLink
              href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
              className="text-center category-item text-lg"
            >
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/video-animation.f0d9d71.svg"
                alt="Video & Animation"
                loading="lazy"
                className="w-12 h-12 block mx-auto mb-4"
              />
              Video &amp; Animation
            </NavLink>
          </li>
          <li>
            <NavLink
              href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
              className="text-center category-item text-lg"
            >
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/music-audio.320af20.svg"
                alt="Music & Audio"
                loading="lazy"
                className="w-12 h-12 block mx-auto mb-4"
              />
              Music &amp; Audio
            </NavLink>
          </li>
          <li>
            <NavLink
              href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
              className="text-center category-item text-lg"
            >
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/programming.9362366.svg"
                alt="Programming & Tech"
                loading="lazy"
                className="w-12 h-12 block mx-auto mb-4"
              />
              Programming &amp; Tech
            </NavLink>
          </li>
          <li>
            <NavLink
              href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
              className="text-center category-item text-lg"
            >
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/business.bbdf319.svg"
                alt="Business"
                loading="lazy"
                className="w-12 h-12 block mx-auto mb-4"
              />
              Business
            </NavLink>
          </li>
          <li>
            <NavLink
              href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
              className="text-center category-item text-lg"
            >
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/lifestyle.745b575.svg"
                alt="Lifestyle"
                loading="lazy"
                className="w-12 h-12 block mx-auto mb-4"
              />
              Lifestyle
            </NavLink>
          </li>
          <li>
            <NavLink
              href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
              className="text-center category-item text-lg"
            >
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/data.718910f.svg"
                alt="Data"
                loading="lazy"
                className="w-12 h-12 block mx-auto mb-4"
              />
              Data
            </NavLink>
          </li>
          <li>
            <NavLink
              href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
              className="text-center category-item text-lg"
            >
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/photography.01cf943.svg"
                alt="Photography"
                loading="lazy"
                className="w-12 h-12 block mx-auto mb-4"
              />
              Photography
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
}
