import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

export default function JobList() {
  const jobArr = useSelector(
    (state) => state.congViecSlice.danhSachCongViecTheoTen
  );

  const renderJobList = (jobArr) => {
    return jobArr.map((item, index) => {
      return (
        <div className="flex justify-center w-full" key={index}>
          <div className="rounded-lg shadow-lg bg-white max-w-sm w-full">
            <div className="img-wrapper w-full">
              <NavLink to={"/detail"}>
                <img
                  className="w-full h-52"
                  src={item.congViec.hinhAnh}
                  alt="true"
                />
              </NavLink>
              {/* Avatar */}
              <div className="seller-info text-body-2 flex items-center">
                <div
                  className="inner-wrapper flex items-center"
                  style={{
                    lineHeight: "120%",
                    padding: "12px 12px 8px",
                    height: "54px",
                    width: "100%",
                  }}
                >
                  <span className="seller-image z-10">
                    <div className="seller-avatar">
                      <img
                        src={item.avatar}
                        alt="seller-avatar"
                        className="w-6 h-6 rounded-full"
                      />
                    </div>
                  </span>
                  <div className="seller-id text-left ml-3">
                    <div className="seller-name hover:underline">
                      <NavLink>
                        <span className="font-medium text-sm">
                          {item.tenNguoiTao}
                        </span>
                      </NavLink>
                    </div>
                    <span className="level">
                      <div
                        className="seller-name-tooltip text-sm"
                        style={{ color: "#FFBE5B" }}
                      >
                        <span className="font-medium">Top Rated Seller</span>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            {/* Introduce */}
            <h3
              className="text-left text-lg hover:text-green-400"
              style={{
                cursor: "pointer",
                lineHeight: "22px",
                height: "43px",
                overFlow: "hidden",
                padding: "0 12px 5px",
                marginTop: "5px",
                wordWrap: "break-word",
                wordBreak: "break-word",
                transition: ".15s ease-in-out",
              }}
            >
              {item.congViec.tenCongViec}
            </h3>

            {/* Star rating */}
            <div class="star-rating flex items-center px-3 py-2">
              <div class="rating-wrapper">
                <span class="gig-rating text-body-2 flex items-center text-yellow-500 font-medium">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 1792 1792"
                    width="15"
                    height="15"
                    className="mr-2"
                  >
                    <path
                      fill="currentColor"
                      d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"
                    ></path>
                  </svg>
                  5.0
                  <span className="ml-1 text-gray-400">
                    ({item.congViec.danhGia})
                  </span>
                </span>
              </div>
            </div>

            {/* Price */}
            <footer className="px-3 py-2">
              <div className="price-wrapper flex items-center justify-between">
                {/* Heart icon */}
                <div className="collect-package">
                  <span className="block" style={{ width: 16, height: 16 }}>
                    <button
                      className="icon-heart"
                      id="icon-heart"
                      delay={120}
                      content=""
                      position="top"
                      boxclassname="_8UHHk5Q"
                      boxcontentclassname="Yn90o2E"
                      containerclassname="collect-package-tooltip"
                    >
                      <span
                        className="heart-icon fill-gray-400 hover:fill-red-500 transition-all"
                        aria-hidden="true"
                        style={{ width: 16, height: 16 }}
                      >
                        <svg
                          width={16}
                          height={16}
                          viewBox="0 0 16 16"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path d="M14.4469 1.95625C12.7344 0.496875 10.1875 0.759375 8.61561 2.38125L7.99999 3.01562L7.38436 2.38125C5.81561 0.759375 3.26561 0.496875 1.55311 1.95625C-0.409388 3.63125 -0.512513 6.6375 1.24374 8.45312L7.29061 14.6969C7.68124 15.1 8.31561 15.1 8.70624 14.6969L14.7531 8.45312C16.5125 6.6375 16.4094 3.63125 14.4469 1.95625Z" />
                        </svg>
                      </span>
                    </button>
                  </span>
                </div>

                {/* Price */}
                <a
                  href="/mightypejes/do-vintage-style-design-and-illustration-for-your-brand?context_referrer=search_gigs&source=main_banner&ref_ctx_id=b31537415431ed340803c36edfe6d065&pckg_id=1&pos=2&context_type=auto&funnel=b31537415431ed340803c36edfe6d065&imp_id=11a6ed09-31e9-4f88-ad7b-ca038cc5439a"
                  target="_blank"
                  className="price"
                >
                  <span className="text-gray-500 font-medium mr-2">
                    Starting at
                  </span>
                  <span className="text-lg font-semibold">
                    {item.congViec.giaTien}$
                  </span>
                </a>
              </div>
            </footer>
          </div>
        </div>
      );
    });
  };

  return (
    <div className="job-list container mx-auto mb-10">
      <div className="job-list-wrapper">
        <div className="content grid grid-cols-4 gap-8 mt-8">
          {renderJobList(jobArr)}
        </div>
      </div>
    </div>
  );
}
