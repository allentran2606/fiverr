import React from "react";
import "./SearchFilter.css";

export default function SearchFilter() {
  const handleOnClick = () => {
    document
      .getElementsByClassName("accordion-button")[0]
      .classList.toggle("is-open");
    document
      .getElementsByClassName("subcategories-list")[0]
      .classList.toggle("is-open");
  };

  const handleOnClickGroup = () => {
    document
      .getElementsByClassName("floating-menu")[0]
      .classList.toggle("open");
  };

  return (
    <div className="container mx-auto">
      <div className="result-for">
        <div className="search-header pt-8">
          <span className="title text-4xl font-bold">Results for "html"</span>
        </div>
      </div>

      <div id="topbar" className="sticky-wrapper">
        <div className="shadow-effect">
          <div className="floating-top-bar flex items-center justify-between">
            <div className="top-filters">
              {/* Button 1 */}
              <div className="top-filters-subcategories-filter as-filter">
                <button className="accordion-button" onClick={handleOnClick}>
                  Category
                  <span
                    className="XQskgrQ chevron-icon-down"
                    aria-hidden="true"
                    style={{ width: 10, height: 10 }}
                  >
                    <svg
                      width={11}
                      height={7}
                      viewBox="0 0 11 7"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M5.4636 6.38899L0.839326 1.769C0.692474 1.62109 0.692474 1.38191 0.839326 1.23399L1.45798 0.61086C1.60483 0.462945 1.84229 0.462945 1.98915 0.61086L5.72919 4.34021L9.46923 0.61086C9.61608 0.462945 9.85354 0.462945 10.0004 0.61086L10.619 1.23399C10.7659 1.38191 10.7659 1.62109 10.619 1.769L5.99477 6.38899C5.84792 6.5369 5.61046 6.5369 5.4636 6.38899Z" />
                    </svg>
                  </span>
                </button>
                <ul className="subcategories-list text-left mt-2 p-5 font-medium">
                  <li className="item mb-2 text-green-400">
                    <a
                      href="https://www.fiverr.com/search/gigs?query=html&source=drop_down_filters&ref_ctx_id=e7f5613b11b208330f1857f7a8e1fc2a&search_in=everywhere&search-autocomplete-original-term=html"
                      className="selected"
                    >
                      All Categories
                    </a>
                  </li>
                  <li className="item py-2 flex items-center justify-between">
                    <a
                      href="https://www.fiverr.com/search/gigs?query=html&source=drop_down_filters&ref_ctx_id=e7f5613b11b208330f1857f7a8e1fc2a&search_in=category&search-autocomplete-original-term=html&sub_category=140&nested_sub_category=2042"
                      className="hover:text-green-400 transition-all"
                    >
                      Web Bug Fixes
                    </a>
                    <span className="text-gray-400"> (1,740) </span>
                  </li>
                  <li className="item  py-2 flex items-center justify-between">
                    <a
                      href="https://www.fiverr.com/search/gigs?query=html&source=drop_down_filters&ref_ctx_id=e7f5613b11b208330f1857f7a8e1fc2a&search_in=category&search-autocomplete-original-term=html&sub_category=140&nested_sub_category=2041"
                      className="hover:text-green-400 transition-all"
                    >
                      Web Application Development
                    </a>
                    <span className="text-gray-400"> (3,115) </span>
                  </li>
                  <li className="item  py-2 flex items-center justify-between">
                    <a
                      href="https://www.fiverr.com/search/gigs?query=html&source=drop_down_filters&ref_ctx_id=e7f5613b11b208330f1857f7a8e1fc2a&search_in=category&search-autocomplete-original-term=html&sub_category=140"
                      className="hover:text-green-400 transition-all"
                    >
                      Web Programming
                    </a>
                    <span className="text-gray-400"> (15,039) </span>
                  </li>
                  <li className="item  py-2 flex items-center justify-between">
                    <a
                      href="https://www.fiverr.com/search/gigs?query=html&source=drop_down_filters&ref_ctx_id=e7f5613b11b208330f1857f7a8e1fc2a&search_in=category&search-autocomplete-original-term=html&sub_category=514"
                      className="hover:text-green-400 transition-all"
                    >
                      Website Development
                    </a>
                    <span className="text-gray-400"> (7,828) </span>
                  </li>
                  <li className="item  py-2 flex items-center justify-between">
                    <a
                      href="https://www.fiverr.com/search/gigs?query=html&source=drop_down_filters&ref_ctx_id=e7f5613b11b208330f1857f7a8e1fc2a&search_in=category&search-autocomplete-original-term=html&sub_category=514&nested_sub_category=2617"
                      className="hover:text-green-400 transition-all"
                    >
                      Custom Websites
                    </a>
                    <span className="text-gray-400"> (6,244) </span>
                  </li>
                  <li className="item  py-2 flex items-center justify-between">
                    <a
                      href="https://www.fiverr.com/search/gigs?query=html&source=drop_down_filters&ref_ctx_id=e7f5613b11b208330f1857f7a8e1fc2a&search_in=category&search-autocomplete-original-term=html&sub_category=140&nested_sub_category=2044"
                      className="hover:text-green-400 transition-all"
                    >
                      PSD to HTML
                    </a>
                    <span className="text-gray-400"> (5,320) </span>
                  </li>
                  <li className="item  py-2 flex items-center justify-between">
                    <a
                      href="https://www.fiverr.com/search/gigs?query=html&source=drop_down_filters&ref_ctx_id=e7f5613b11b208330f1857f7a8e1fc2a&search_in=category&search-autocomplete-original-term=html&sub_category=56"
                      className="hover:text-green-400 transition-all"
                    >
                      Business Cards &amp; Stationery
                    </a>
                    <span className="text-gray-400"> (1,726) </span>
                  </li>
                  <li className="item  py-2 flex items-center justify-between">
                    <a
                      href="https://www.fiverr.com/search/gigs?query=html&source=drop_down_filters&ref_ctx_id=e7f5613b11b208330f1857f7a8e1fc2a&search_in=category&search-autocomplete-original-term=html&sub_category=140&nested_sub_category=2046"
                      className="hover:text-green-400 transition-all"
                    >
                      Landing Page Development
                    </a>
                    <span className="text-gray-400"> (1,322) </span>
                  </li>
                  <li className="item  py-2 flex items-center justify-between">
                    <a
                      href="https://www.fiverr.com/search/gigs?query=html&source=drop_down_filters&ref_ctx_id=e7f5613b11b208330f1857f7a8e1fc2a&search_in=category&search-autocomplete-original-term=html&sub_category=151"
                      className="hover:text-green-400 transition-all"
                    >
                      Website Design
                    </a>
                    <span className="text-gray-400"> (1,282) </span>
                  </li>
                  <li className="item  py-2 flex items-center justify-between">
                    <a
                      href="https://www.fiverr.com/search/gigs?query=html&source=drop_down_filters&ref_ctx_id=e7f5613b11b208330f1857f7a8e1fc2a&search_in=category&search-autocomplete-original-term=html&sub_category=514&nested_sub_category=2609"
                      className="hover:text-green-400 transition-all"
                    >
                      WordPress
                    </a>
                    <span className="text-gray-400"> (1,045) </span>
                  </li>
                </ul>
              </div>

              {/* Button 2 */}
              <div
                className="floating-menu grouped"
                onClick={handleOnClickGroup}
              >
                <div className="menu-title more-filter-menu">
                  Service Options
                  <span
                    className="XQskgrQ chevron-icon-down"
                    aria-hidden="true"
                    style={{ width: 10, height: 10 }}
                  >
                    <svg
                      width={11}
                      height={7}
                      viewBox="0 0 11 7"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M5.4636 6.38899L0.839326 1.769C0.692474 1.62109 0.692474 1.38191 0.839326 1.23399L1.45798 0.61086C1.60483 0.462945 1.84229 0.462945 1.98915 0.61086L5.72919 4.34021L9.46923 0.61086C9.61608 0.462945 9.85354 0.462945 10.0004 0.61086L10.619 1.23399C10.7659 1.38191 10.7659 1.62109 10.619 1.769L5.99477 6.38899C5.84792 6.5369 5.61046 6.5369 5.4636 6.38899Z" />
                    </svg>
                  </span>
                </div>

                <div className="menu-content text-lg">
                  <div className="content-scroll" style={{ maxHeight: 559 }}>
                    <div>
                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Programming language{" "}
                        </div>
                        <div className="checkbox-list text-left">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="html_css"
                              data-testid="checkbox-filter-html_css"
                              defaultValue="html_css"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 87 }}>
                                <span>HTML &amp; CSS</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,099)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="php"
                              data-testid="checkbox-filter-php"
                              defaultValue="php"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 30 }}>
                                <span>PHP</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (309)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="javascript"
                              data-testid="checkbox-filter-javascript"
                              defaultValue="javascript"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 72 }}>
                                <span>JavaScript</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (202)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="other"
                              data-testid="checkbox-filter-other"
                              defaultValue="other"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 41 }}>
                                <span>Other</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (9)
                              </span>
                            </div>
                          </label>
                        </div>
                        <p className="show-more-less text-left font-medium text-green-400">
                          +6 more
                        </p>
                      </div>

                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Expertise{" "}
                        </div>
                        <div className="checkbox-list text-left">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="performance"
                              data-testid="checkbox-filter-performance"
                              defaultValue="performance"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 89 }}>
                                <span>Performance</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (640)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="design"
                              data-testid="checkbox-filter-design"
                              defaultValue="design"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 47 }}>
                                <span>Design</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (627)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="from_psd_file"
                              data-testid="checkbox-filter-from_psd_file"
                              defaultValue="from_psd_file"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 92 }}>
                                <span>PSD to HTML</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (611)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox extra-item">
                            <input
                              type="checkbox"
                              name="w3c_validation"
                              data-testid="checkbox-filter-w3c_validation"
                              defaultValue="w3c_validation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label">
                                <span>W3C validation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (416)
                              </span>
                            </div>
                          </label>
                        </div>
                        <p className="show-more-less text-left font-medium text-green-400">
                          +4 more
                        </p>
                      </div>

                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Service includes{" "}
                        </div>
                        <div className="checkbox-list">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="bug_investigation"
                              data-testid="checkbox-filter-bug_investigation"
                              defaultValue="bug_investigation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 116 }}>
                                <span>Bug investigation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,354)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="detailed_code_comments"
                              data-testid="checkbox-filter-detailed_code_comments"
                              defaultValue="detailed_code_comments"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 172 }}>
                                <span>Detailed code comments</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,244)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="fix_documentation"
                              data-testid="checkbox-filter-fix_documentation"
                              defaultValue="fix_documentation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 127 }}>
                                <span>Fix documentation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,102)
                              </span>
                            </div>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="button-row flex items-center justify-between px-6 py-4">
                    <a className="clear-all text-gray-400 font-medium">
                      Clear All
                    </a>
                    <a className="apply px-3 py-1 text-white bg-green-400 rounded-md font-medium">
                      Apply
                    </a>
                  </div>
                </div>
              </div>

              {/* Button 3 */}
              <div
                className="floating-menu grouped"
                onClick={handleOnClickGroup}
              >
                <div className="menu-title more-filter-menu">
                  Seller Details
                  <span
                    className="XQskgrQ chevron-icon-down"
                    aria-hidden="true"
                    style={{ width: 10, height: 10 }}
                  >
                    <svg
                      width={11}
                      height={7}
                      viewBox="0 0 11 7"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M5.4636 6.38899L0.839326 1.769C0.692474 1.62109 0.692474 1.38191 0.839326 1.23399L1.45798 0.61086C1.60483 0.462945 1.84229 0.462945 1.98915 0.61086L5.72919 4.34021L9.46923 0.61086C9.61608 0.462945 9.85354 0.462945 10.0004 0.61086L10.619 1.23399C10.7659 1.38191 10.7659 1.62109 10.619 1.769L5.99477 6.38899C5.84792 6.5369 5.61046 6.5369 5.4636 6.38899Z" />
                    </svg>
                  </span>
                </div>

                <div className="menu-content text-lg">
                  <div className="content-scroll" style={{ maxHeight: 559 }}>
                    <div>
                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Programming language{" "}
                        </div>
                        <div className="checkbox-list text-left">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="html_css"
                              data-testid="checkbox-filter-html_css"
                              defaultValue="html_css"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 87 }}>
                                <span>HTML &amp; CSS</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,099)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="php"
                              data-testid="checkbox-filter-php"
                              defaultValue="php"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 30 }}>
                                <span>PHP</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (309)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="javascript"
                              data-testid="checkbox-filter-javascript"
                              defaultValue="javascript"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 72 }}>
                                <span>JavaScript</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (202)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="other"
                              data-testid="checkbox-filter-other"
                              defaultValue="other"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 41 }}>
                                <span>Other</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (9)
                              </span>
                            </div>
                          </label>
                        </div>
                        <p className="show-more-less text-left font-medium text-green-400">
                          +6 more
                        </p>
                      </div>

                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Expertise{" "}
                        </div>
                        <div className="checkbox-list text-left">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="performance"
                              data-testid="checkbox-filter-performance"
                              defaultValue="performance"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 89 }}>
                                <span>Performance</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (640)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="design"
                              data-testid="checkbox-filter-design"
                              defaultValue="design"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 47 }}>
                                <span>Design</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (627)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="from_psd_file"
                              data-testid="checkbox-filter-from_psd_file"
                              defaultValue="from_psd_file"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 92 }}>
                                <span>PSD to HTML</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (611)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox extra-item">
                            <input
                              type="checkbox"
                              name="w3c_validation"
                              data-testid="checkbox-filter-w3c_validation"
                              defaultValue="w3c_validation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label">
                                <span>W3C validation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (416)
                              </span>
                            </div>
                          </label>
                        </div>
                        <p className="show-more-less text-left font-medium text-green-400">
                          +4 more
                        </p>
                      </div>

                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Service includes{" "}
                        </div>
                        <div className="checkbox-list">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="bug_investigation"
                              data-testid="checkbox-filter-bug_investigation"
                              defaultValue="bug_investigation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 116 }}>
                                <span>Bug investigation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,354)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="detailed_code_comments"
                              data-testid="checkbox-filter-detailed_code_comments"
                              defaultValue="detailed_code_comments"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 172 }}>
                                <span>Detailed code comments</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,244)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="fix_documentation"
                              data-testid="checkbox-filter-fix_documentation"
                              defaultValue="fix_documentation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 127 }}>
                                <span>Fix documentation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,102)
                              </span>
                            </div>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="button-row flex items-center justify-between px-6 py-4">
                    <a className="clear-all text-gray-400 font-medium">
                      Clear All
                    </a>
                    <a className="apply px-3 py-1 text-white bg-green-400 rounded-md font-medium">
                      Apply
                    </a>
                  </div>
                </div>
              </div>

              {/* Button 4 */}
              <div
                className="floating-menu grouped"
                onClick={handleOnClickGroup}
              >
                <div className="menu-title more-filter-menu">
                  Budget
                  <span
                    className="XQskgrQ chevron-icon-down"
                    aria-hidden="true"
                    style={{ width: 10, height: 10 }}
                  >
                    <svg
                      width={11}
                      height={7}
                      viewBox="0 0 11 7"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M5.4636 6.38899L0.839326 1.769C0.692474 1.62109 0.692474 1.38191 0.839326 1.23399L1.45798 0.61086C1.60483 0.462945 1.84229 0.462945 1.98915 0.61086L5.72919 4.34021L9.46923 0.61086C9.61608 0.462945 9.85354 0.462945 10.0004 0.61086L10.619 1.23399C10.7659 1.38191 10.7659 1.62109 10.619 1.769L5.99477 6.38899C5.84792 6.5369 5.61046 6.5369 5.4636 6.38899Z" />
                    </svg>
                  </span>
                </div>

                <div className="menu-content text-lg">
                  <div className="content-scroll" style={{ maxHeight: 559 }}>
                    <div>
                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Programming language{" "}
                        </div>
                        <div className="checkbox-list text-left">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="html_css"
                              data-testid="checkbox-filter-html_css"
                              defaultValue="html_css"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 87 }}>
                                <span>HTML &amp; CSS</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,099)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="php"
                              data-testid="checkbox-filter-php"
                              defaultValue="php"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 30 }}>
                                <span>PHP</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (309)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="javascript"
                              data-testid="checkbox-filter-javascript"
                              defaultValue="javascript"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 72 }}>
                                <span>JavaScript</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (202)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="other"
                              data-testid="checkbox-filter-other"
                              defaultValue="other"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 41 }}>
                                <span>Other</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (9)
                              </span>
                            </div>
                          </label>
                        </div>
                        <p className="show-more-less text-left font-medium text-green-400">
                          +6 more
                        </p>
                      </div>

                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Expertise{" "}
                        </div>
                        <div className="checkbox-list text-left">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="performance"
                              data-testid="checkbox-filter-performance"
                              defaultValue="performance"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 89 }}>
                                <span>Performance</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (640)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="design"
                              data-testid="checkbox-filter-design"
                              defaultValue="design"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 47 }}>
                                <span>Design</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (627)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="from_psd_file"
                              data-testid="checkbox-filter-from_psd_file"
                              defaultValue="from_psd_file"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 92 }}>
                                <span>PSD to HTML</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (611)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox extra-item">
                            <input
                              type="checkbox"
                              name="w3c_validation"
                              data-testid="checkbox-filter-w3c_validation"
                              defaultValue="w3c_validation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label">
                                <span>W3C validation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (416)
                              </span>
                            </div>
                          </label>
                        </div>
                        <p className="show-more-less text-left font-medium text-green-400">
                          +4 more
                        </p>
                      </div>

                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Service includes{" "}
                        </div>
                        <div className="checkbox-list">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="bug_investigation"
                              data-testid="checkbox-filter-bug_investigation"
                              defaultValue="bug_investigation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 116 }}>
                                <span>Bug investigation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,354)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="detailed_code_comments"
                              data-testid="checkbox-filter-detailed_code_comments"
                              defaultValue="detailed_code_comments"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 172 }}>
                                <span>Detailed code comments</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,244)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="fix_documentation"
                              data-testid="checkbox-filter-fix_documentation"
                              defaultValue="fix_documentation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 127 }}>
                                <span>Fix documentation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,102)
                              </span>
                            </div>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="button-row flex items-center justify-between px-6 py-4">
                    <a className="clear-all text-gray-400 font-medium">
                      Clear All
                    </a>
                    <a className="apply px-3 py-1 text-white bg-green-400 rounded-md font-medium">
                      Apply
                    </a>
                  </div>
                </div>
              </div>

              {/* Button 5 */}
              <div
                className="floating-menu grouped"
                onClick={handleOnClickGroup}
              >
                <div className="menu-title more-filter-menu">
                  Delivery Times
                  <span
                    className="XQskgrQ chevron-icon-down"
                    aria-hidden="true"
                    style={{ width: 10, height: 10 }}
                  >
                    <svg
                      width={11}
                      height={7}
                      viewBox="0 0 11 7"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M5.4636 6.38899L0.839326 1.769C0.692474 1.62109 0.692474 1.38191 0.839326 1.23399L1.45798 0.61086C1.60483 0.462945 1.84229 0.462945 1.98915 0.61086L5.72919 4.34021L9.46923 0.61086C9.61608 0.462945 9.85354 0.462945 10.0004 0.61086L10.619 1.23399C10.7659 1.38191 10.7659 1.62109 10.619 1.769L5.99477 6.38899C5.84792 6.5369 5.61046 6.5369 5.4636 6.38899Z" />
                    </svg>
                  </span>
                </div>

                <div className="menu-content text-lg">
                  <div className="content-scroll" style={{ maxHeight: 559 }}>
                    <div>
                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Programming language{" "}
                        </div>
                        <div className="checkbox-list text-left">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="html_css"
                              data-testid="checkbox-filter-html_css"
                              defaultValue="html_css"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 87 }}>
                                <span>HTML &amp; CSS</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,099)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="php"
                              data-testid="checkbox-filter-php"
                              defaultValue="php"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 30 }}>
                                <span>PHP</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (309)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="javascript"
                              data-testid="checkbox-filter-javascript"
                              defaultValue="javascript"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 72 }}>
                                <span>JavaScript</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (202)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="other"
                              data-testid="checkbox-filter-other"
                              defaultValue="other"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 41 }}>
                                <span>Other</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (9)
                              </span>
                            </div>
                          </label>
                        </div>
                        <p className="show-more-less text-left font-medium text-green-400">
                          +6 more
                        </p>
                      </div>

                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Expertise{" "}
                        </div>
                        <div className="checkbox-list text-left">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="performance"
                              data-testid="checkbox-filter-performance"
                              defaultValue="performance"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 89 }}>
                                <span>Performance</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (640)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="design"
                              data-testid="checkbox-filter-design"
                              defaultValue="design"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 47 }}>
                                <span>Design</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (627)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="from_psd_file"
                              data-testid="checkbox-filter-from_psd_file"
                              defaultValue="from_psd_file"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 92 }}>
                                <span>PSD to HTML</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (611)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox extra-item">
                            <input
                              type="checkbox"
                              name="w3c_validation"
                              data-testid="checkbox-filter-w3c_validation"
                              defaultValue="w3c_validation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label">
                                <span>W3C validation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (416)
                              </span>
                            </div>
                          </label>
                        </div>
                        <p className="show-more-less text-left font-medium text-green-400">
                          +4 more
                        </p>
                      </div>

                      <div className="more-filter-item with-carrot">
                        <div className="content-title text-left">
                          {" "}
                          Service includes{" "}
                        </div>
                        <div className="checkbox-list">
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="bug_investigation"
                              data-testid="checkbox-filter-bug_investigation"
                              defaultValue="bug_investigation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 116 }}>
                                <span>Bug investigation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,354)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="detailed_code_comments"
                              data-testid="checkbox-filter-detailed_code_comments"
                              defaultValue="detailed_code_comments"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 172 }}>
                                <span>Detailed code comments</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,244)
                              </span>
                            </div>
                          </label>
                          <label className="-SSRhMt zsZmoTB cWwLjTL checkbox">
                            <input
                              type="checkbox"
                              name="fix_documentation"
                              data-testid="checkbox-filter-fix_documentation"
                              defaultValue="fix_documentation"
                            />

                            <div className="inner-checkbox ml-2">
                              <div className="label" style={{ width: 127 }}>
                                <span>Fix documentation</span>
                              </div>
                              <span className="count text-gray-400 ml-1">
                                (1,102)
                              </span>
                            </div>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="button-row flex items-center justify-between px-6 py-4">
                    <a className="clear-all text-gray-400 font-medium">
                      Clear All
                    </a>
                    <a className="apply px-3 py-1 text-white bg-green-400 rounded-md font-medium">
                      Apply
                    </a>
                  </div>
                </div>
              </div>
            </div>

            {/* Toggle */}
            <div className="togglers-wrapper flex flex-items-baseline mt-4 space-x-3">
              <label
                for="Toggle1"
                className="inline-flex items-center space-x-4 cursor-pointer"
              >
                <span className="relative">
                  <input id="Toggle1" type="checkbox" className="hidden peer" />
                  <div className="w-10 h-6 rounded-full shadow-inner bg-gray-300 peer-checked:bg-green-400"></div>
                  <div className="absolute inset-y-0 left-0 w-4 h-4 m-1 rounded-full shadow peer-checked:right-0 peer-checked:left-auto dark:bg-white"></div>
                </span>
                <span className="text-left font-medium text-gray-500">
                  Pro services
                </span>
              </label>
              <label
                for="Toggle2"
                className="inline-flex items-center space-x-4 cursor-pointer"
              >
                <span className="relative">
                  <input id="Toggle2" type="checkbox" className="hidden peer" />
                  <div className="w-10 h-6 rounded-full shadow-inner bg-gray-300 peer-checked:bg-green-400"></div>
                  <div className="absolute inset-y-0 left-0 w-4 h-4 m-1 rounded-full shadow peer-checked:right-0 peer-checked:left-auto dark:bg-white"></div>
                </span>
                <span className="text-left font-medium text-gray-500">
                  Local sellers
                </span>
              </label>
              <label
                for="Toggle3"
                className="inline-flex items-center space-x-4 cursor-pointer"
              >
                <span className="relative">
                  <input id="Toggle3" type="checkbox" className="hidden peer" />
                  <div className="w-10 h-6 rounded-full shadow-inner bg-gray-300 peer-checked:bg-green-400"></div>
                  <div className="absolute inset-y-0 left-0 w-4 h-4 m-1 rounded-full shadow peer-checked:right-0 peer-checked:left-auto dark:bg-white"></div>
                </span>
                <span className="text-left font-medium text-gray-500">
                  Online Sellers
                </span>
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
