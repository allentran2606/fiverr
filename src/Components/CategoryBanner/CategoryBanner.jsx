import React from "react";

export default function CategoryBanner() {
  return (
    <div className="container mx-auto">
      <div className="category-banner-wrapper">
        <div
          className="hero-banner-wrapper"
          style={{
            backgroundColor: "rgb(11,58,35)",
            backgroundImage:
              "url(https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/3f1b7ea10295936b6846bcff0afd38cf-1626595415203/graphics-design-desktop.png)",
            backgroundSize: "cover",
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            borderRadius: "6px",
          }}
        >
          <div className="banner-content text-center text-white p-16">
            <h1 className="text-3xl font-bold">Graphics & Design</h1>
            <p className="text-xl mt-2">Designs to make you stand out.</p>
            <div className="banner-button flex items-center justify-center mt-6">
              <button className="flex items-center text-lg border-2 border-white rounded-md px-4 py-3 hover:bg-white hover:text-black hover:fill-black ">
                How Fiverr Works
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
