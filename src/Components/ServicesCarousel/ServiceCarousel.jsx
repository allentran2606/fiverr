import React from "react";
import { Desktop, Laptop, Mobile, Tablet } from "../../HOC/Responsive";
import ServicesCarouselDesktop from "./ServicesCarouselDesktop";
import ServicesCarouselLaptop from "./ServicesCarouselLaptop";
import ServicesCarouselMobile from "./ServicesCarouselMobile";
import ServicesCarouselTablet from "./ServicesCarouselTablet";

export default function ServiceCarousel() {
  return (
    <div className="services-carousel">
      <Desktop>
        <ServicesCarouselDesktop />
      </Desktop>
      <Laptop>
        <ServicesCarouselLaptop />
      </Laptop>
      <Tablet>
        <ServicesCarouselTablet />
      </Tablet>
      <Mobile>
        <ServicesCarouselMobile />
      </Mobile>
    </div>
  );
}
