import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";

export default function Selling() {
  return (
    <div className="selling container mx-auto pb-24 my-20">
      <div className="selling-wrapper">
        <div className="grid-15 flex items-center">
          <div className="left-section text-left lg:pr-44 lg:w-1/2">
            <h2 className="text-4xl pb-6 font-semibold text-gray-700">
              A whole world of freelance talent at your fingertips
            </h2>
            <ul>
              <li className="pb-6">
                <h6 className="flex items-start text-xl font-semibold pb-2">
                  <span
                    className="mr-2"
                    aria-hidden="true"
                    style={{
                      width: 24,
                      height: 24,
                      fill: "rgb(122, 125, 133)",
                    }}
                  >
                    <FontAwesomeIcon icon={faCheckCircle} />
                  </span>
                  The best for every budget
                </h6>
                <p className="tbody-4 text-lg text-gray-500 font-medium">
                  Find high-quality services at every price point. No hourly
                  rates, just project-based pricing.
                </p>
              </li>
              <li className="pb-6">
                <h6 className="flex items-start text-xl font-semibold pb-2">
                  <span
                    className="mr-2"
                    aria-hidden="true"
                    style={{
                      width: 24,
                      height: 24,
                      fill: "rgb(122, 125, 133)",
                    }}
                  >
                    <FontAwesomeIcon icon={faCheckCircle} />
                  </span>
                  Quality work done quickly
                </h6>
                <p className="tbody-4 text-lg text-gray-500 font-medium">
                  Find the right freelancer to begin working on your project
                  within minutes.
                </p>
              </li>
              <li className="pb-6">
                <h6 className="flex items-start text-xl font-semibold pb-2">
                  <span
                    className=" mr-2"
                    aria-hidden="true"
                    style={{
                      width: 24,
                      height: 24,
                      fill: "rgb(122, 125, 133)",
                    }}
                  >
                    <FontAwesomeIcon icon={faCheckCircle} />
                  </span>
                  Protected payments, every time
                </h6>
                <p className="tbody-4 text-lg text-gray-500 font-medium">
                  Always know what you'll pay upfront. Your payment isn't
                  released until you approve the work.
                </p>
              </li>
              <li className="pb-6">
                <h6 className="flex items-start text-xl font-semibold pb-2">
                  <span
                    className="mr-2"
                    aria-hidden="true"
                    style={{
                      width: 24,
                      height: 24,
                      fill: "rgb(122, 125, 133)",
                    }}
                  >
                    <FontAwesomeIcon icon={faCheckCircle} />
                  </span>
                  24/7 support
                </h6>
                <p className="tbody-4 text-lg text-gray-500 font-medium">
                  Questions? Our round-the-clock support team is available to
                  help anytime, anywhere.
                </p>
              </li>
            </ul>
          </div>
          <div className="right-section lg:w-1/2">
            <div className="picture-wrapper w-full h-full">
              <img
                src="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_600,dpr_2.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
