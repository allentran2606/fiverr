import axios from "axios";
import { createConfig, DOMAIN } from "../utils/config";

export const userService = {
  layThongTinUserDangNhap: (dataUser) => {
    return axios({
      data: dataUser,
      url: `${DOMAIN}/api/auth/signin`,
      method: "POST",
      headers: createConfig(),
    });
  },
};
