import axios from "axios";
import { DOMAIN, createConfig } from "../utils/config";

export const congViecService = {
  layDanhSachCongViecTheoTen: (tenCongViec) => {
    return axios({
      url: `${DOMAIN}/api/cong-viec/lay-danh-sach-cong-viec-theo-ten/${tenCongViec}`,
      method: "GET",
      data: tenCongViec,
      headers: createConfig(),
    });
  },

  layMenuLoaiCongViec: () => {
    return axios({
      url: `${DOMAIN}/api/cong-viec/lay-menu-loai-cong-viec`,
      method: "GET",
      headers: createConfig(),
    });
  },

  layCongViecTheoChiTietLoai: (maLoaiCongViec) => {
    return axios({
      url: `${DOMAIN}/api/cong-viec/lay-chi-tiet-loai-cong-viec/${maLoaiCongViec}`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
