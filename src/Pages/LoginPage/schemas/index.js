import * as yup from "yup";

const passwordRules = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}$/;
// min 5 characters, 1 upper case letter, 1 lower case letter, 1 numeric digit.

export const basicSchema = yup.object().shape({
  email: yup
    .string()
    .email("Please enter a valid email")
    .required("You must enter an email"),
  password: yup
    .string()
    .min(5, "Password is at least 5 characters")
    .matches(passwordRules, {
      message:
        "Password must contain 1 uppercase letter, 1 lowercase letter and 1 number",
    })
    .required("You must enter a password"),
});
